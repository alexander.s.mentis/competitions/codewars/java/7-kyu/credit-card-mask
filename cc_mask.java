public class Maskify {
    public static String maskify(String str) {
        
        int strlen = str.length();
        
        try {
            return String.join("", "#".repeat(strlen - 4), str.substring(strlen - 4));
        }
        catch(IllegalArgumentException | IndexOutOfBoundsException e) {
            return str;
        }
    }
}